# Copyright 2021 Offensive Security
# SPDX-license-identifier: GPL-3.0-only

import glob
import os
import shutil
import tempfile
from collections import namedtuple
from urllib.parse import urlparse, urlunparse

from kali_tweaks.utils import (
    apt_update,
    logger,
    run_as_root,
    write_file_as_root,
)

# THINGS TO TEST IN AUTOPKGTEST
# - failures of apt-update, backup files should be restored
# - an extra suite might be defined either in sources.list,
#   either in sources.list.d/whatever.list. In both cases,
#   disabling it should work.
# - when removing extra suite that is defined in its own file,
#   it shouldn't leave an empty file, but instead delete the file.
# - adding extra suite should overwrite any existing target file.

KALI_DEFAULT_MIRROR = "http.kali.org"
KALI_DEFAULT_PROTOCOL = "http"

KALI_SUPPORTED_MIRRORS = [
    "http.kali.org",
    "kali.download",
]

KALI_SUPPORTED_PROTOCOLS = [
    "http",
    "https",
]

KALI_SUITES = [
    "kali-bleeding-edge",
    "kali-dev",
    "kali-experimental",
    "kali-last-snapshot",
    "kali-rolling",
]


Source = namedtuple("Source", "type, options, uri, suite, components")


def parse_kali_source(line, discard_src=False):
    """
    Parse a line from a sources.list file.

    If this line is a Kali source, return a named tuple. If it is not
    a Kali source, return None. If the line is malformed, raise
    ValueError.

    What's the criteria to identify a "Kali source"? This is based on
    the suite name: if it's a known Kali suite (eg. kali-rolling,
    kali-dev, etc...) then it's a Kali source. Obviously this is not
    perfect: if ever a third-party provides packages for Kali via an
    APT repository, and uses a known Kali suite name, then it will be
    recognized a Kali source.

    For reference regarding the format of sources.list file:
    https://manpages.debian.org/unstable/apt/sources.list.5.en.html
    """

    line = line.strip()

    if not line or line.startswith("#"):
        return None

    source_type, line = line.split(maxsplit=1)
    if source_type not in ["deb", "deb-src"]:
        raise ValueError
    if source_type == "deb-src" and discard_src is True:
        return None

    options = None
    if line.startswith("["):
        idx = line.index("]")
        options = line[0 : idx + 1]
        line = line[idx + 1 :].lstrip()

    uri, suite, components = line.split(maxsplit=2)

    if suite not in KALI_SUITES:
        return None

    uri = urlparse(uri)

    return Source(source_type, options, uri, suite, components)


def print_kali_source(source):
    """
    Print a Kali source to a string.

    This line is meant to be added to a sources.list file.
    """

    parts = []

    parts.append(source.type)
    if source.options:
        parts.append(source.options)
    parts.append(urlunparse(source.uri))
    parts.append(source.suite)
    parts.append(source.components)

    return " ".join(parts)


def parse_sources_list(content, discard_src=False):
    """
    Parse the content from a sources.list file, return the Kali sources found.
    """

    sources = []

    for line in content.splitlines():
        try:
            source = parse_kali_source(line, discard_src=discard_src)
        except ValueError:
            continue
        if source:
            sources.append(source)

    return sources


def update_sources_list(content, mirror=None, protocol=None, remove_suites=[]):
    """
    Parse the content from a sources.list file, and modify it according to
    the arguments.

    Return a tuple with a modified content, and a boolean that says whether
    the content was modified or not.
    """

    content_modified = False
    output = []

    for idx, line in enumerate(content.splitlines()):
        try:
            source = parse_kali_source(line)
        except ValueError:
            output.append(line)
            continue

        if not source:
            output.append(line)
            continue

        if source.suite in remove_suites:
            content_modified = True
            continue

        uri = source.uri

        if protocol and protocol != uri.scheme:
            uri = uri._replace(scheme=protocol)

        if mirror and mirror != uri.netloc:
            uri = uri._replace(
                netloc=mirror, path="kali", params="", query="", fragment=""
            )

        if uri != source.uri:
            source = source._replace(uri=uri)
            line = print_kali_source(source)
            content_modified = True

        output.append(line)

    if content_modified:
        content = "\n".join(output) + "\n" if output else ""

    return content, content_modified


class TempBackup:
    """
    Class to backup files in /tmp, then either restore those backups
    all at once, or delete them all.
    """

    def __init__(self):
        self.backups = []

    def backup(self, file):
        """
        Create a backup of a file as a temporary file.
        """
        fd, temp_path = tempfile.mkstemp()
        os.close(fd)
        shutil.copy2(file, temp_path)
        self.backups += [(file, temp_path)]
        logger.debug("Backed up %s -> %s", file, temp_path)

    def restore_all(self):
        """
        Restore all backups.
        """
        for orig, bck in self.backups:
            try:
                shutil.copy2(bck, orig)
            except PermissionError:
                cmd = f"cp --preserve=all {bck} {orig}"
                res = run_as_root(cmd, interactive=True, log=False)
                if res.returncode != 0:
                    print(f"Couldn't restore file '{orig}'!")
                    print(f"Original file at '{bck}', please restore it manually.")
                    continue
            os.remove(bck)
        self.backups = []

    def remove_all(self):
        """
        Remove all backup files.
        """
        for _, bck in self.backups:
            os.remove(bck)
        self.backups = []


class AptRepositoriesSetting:
    def __init__(self):
        self.mirror = None
        self.protocol = None
        self.extra_suites = {}

    def _get_source_files(self):
        source_files = ["/etc/apt/sources.list"]
        source_files += glob.glob("/etc/apt/sources.list.d/*.list")
        return source_files

    def _load(self):
        """
        Load the various sources.list files, and find out how Kali
        repositories are configured.

        Only the binary sources are considered (ie. we don't care
        about the deb-src lines).
        """
        sources = []

        # Read and parse all sources.list files
        source_files = self._get_source_files()
        for fn in source_files:
            with open(fn) as f:
                content = f.read()
            sources += parse_sources_list(content, discard_src=True)

        # Detect the current mirror and protocol. Leave it to None
        # if more than one is in use, or if it's not something that
        # we expect.
        self.mirror = None
        self.protocol = None
        netlocs = list(set([s.uri.netloc for s in sources]))
        schemes = list(set([s.uri.scheme for s in sources]))
        mirror = netlocs[0] if len(netlocs) == 1 else None
        proto = schemes[0] if len(schemes) == 1 else None
        if mirror and mirror in KALI_SUPPORTED_MIRRORS:
            self.mirror = mirror
        if proto and proto in KALI_SUPPORTED_PROTOCOLS:
            self.protocol = proto

        # Detect if any extra suites are enabled.
        self.extra_suites = {}
        for suite in ["kali-bleeding-edge", "kali-experimental"]:
            enabled = any([s.suite == suite for s in sources])
            if enabled:
                self.extra_suites[suite] = True
            else:
                self.extra_suites[suite] = False

    def load(self):
        """
        Load the various sources.list files.

        Return a dict showing how Kali is configured, based on the
        binary sources only (ie. deb-src lines are ignored).
        """
        self._load()

        return {
            "extra-repos": dict(self.extra_suites),
            "mirror": self.mirror,
            "protocol": self.protocol,
        }

    def save(self, config):
        """
        Save changes.
        """

        self._load()

        # A special case: if caller wants to set the protocol to
        # HTTPS, and the mirror is neither http.kali.org neither
        # kali.download, then we force the mirror to a default.
        # This is because we can't just set https for an unknown
        # host, as we have no idea if this host supports https.
        if "protocol" in config and config["protocol"] == "https":
            if "mirror" not in config and self.mirror is None:
                config["mirror"] = KALI_DEFAULT_MIRROR

        # We're going to backup every file that we touch
        backups = TempBackup()

        # Apply the updates, which means:
        # - remove repositories if requested
        # - update mirror and protocol if requested
        new_mirror = config.get("mirror", None)
        new_proto = config.get("protocol", None)
        suites_to_add = []
        suites_to_remove = []
        if "extra-repos" in config:
            for repo, value in config["extra-repos"].items():
                if value is True:
                    suites_to_add.append(repo)
                else:
                    suites_to_remove.append(repo)

        source_files = self._get_source_files()
        for fn in source_files:
            # Read file
            with open(fn) as f:
                content = f.read()

            # Update content
            content, modified = update_sources_list(
                content,
                mirror=new_mirror,
                protocol=new_proto,
                remove_suites=suites_to_remove,
            )
            if modified is False:
                continue

            # Check if new content is actually empty
            content_empty = True
            for line in content:
                if line.strip() != "":
                    content_empty = False
                    break
            if content_empty:
                content = None

            # Write changes
            backups.backup(fn)
            try:
                if content:
                    print(f"> Writing changes to {fn}")
                    write_file_as_root(fn, content)
                else:
                    print(f"> Removing file {fn}")
                    run_as_root(f"rm {fn}", log=False)
            except RuntimeError:
                print("> Error, restoring original sources.list files")
                backups.restore_all()
                raise

        # Add repositories
        for suite in suites_to_add:
            mirror = new_mirror if new_mirror else self.mirror
            if not mirror:
                mirror = KALI_DEFAULT_MIRROR
            proto = new_proto if new_proto else self.protocol
            if not proto:
                proto = KALI_DEFAULT_PROTOCOL
            components = "main contrib non-free"
            content = f"deb {proto}://{mirror}/kali {suite} {components}"
            target = f"/etc/apt/sources.list.d/{suite}.list"
            print(f"> New repository: {content}")
            print(f"> Installing to: '{target}'")
            try:
                write_file_as_root(target, content)
            except RuntimeError:
                print("> Error, restoring original sources.list files")
                backups.restore_all()
                raise

        # No file was modified?
        if not backups.backups and not suites_to_add:
            return

        # Run apt update
        try:
            apt_update()
        except RuntimeError:
            print("> Error, restoring original sources.list files")
            backups.restore_all()
            raise

        # Everything went fine, remove backup files
        backups.remove_all()
