# Copyright 2022 Offensive Security
# SPDX-license-identifier: GPL-3.0-only

import os

from kali_tweaks.utils import (
    run_as_root,
    say,
    write_file_as_root,
)

KALI_SSH_WIDE_COMPAT_CONTENT = """\
# This file was added by kali-tweaks. Please use the kali-tweaks
# tool if you want to disable it.
#
# The configuration below enables legacy ciphers and algorithms,
# to allow interacting with old servers that still use those.

Host *
    Ciphers +3des-cbc,aes128-cbc,aes192-cbc,aes256-cbc
    KexAlgorithms +diffie-hellman-group-exchange-sha1,diffie-hellman-group1-sha1,diffie-hellman-group14-sha1
    HostKeyAlgorithms +ssh-rsa,ssh-rsa-cert-v01@openssh.com,ssh-dss,ssh-dss-cert-v01@openssh.com
    PubkeyAcceptedAlgorithms +ssh-rsa,ssh-rsa-cert-v01@openssh.com,ssh-dss,ssh-dss-cert-v01@openssh.com
    LocalCommand /bin/echo "Warning: SSH client configured for wide compatibility by kali-tweaks."
    PermitLocalCommand yes
"""  # noqa


class SSHSetting:
    def __init__(self):
        self.kali_conf = "/etc/ssh/ssh_config.d/kali-wide-compat.conf"

    def load(self):
        fn = self.kali_conf
        if os.path.isfile(fn):
            value = "compat"
        else:
            value = "secure"
        return {"hardening": value}

    def apply(self, config):
        say("Configuring SSH")
        if "hardening" in config:
            value = config["hardening"]
            if value == "compat":
                self.set_wide_compat()
            elif value == "secure":
                self.unset_wide_compat()
            else:
                raise NotImplementedError(f"'{value}' not supported")

    def set_wide_compat(self):
        fn = self.kali_conf
        print("> Enabling wide compatibility")
        print(f"> Writing changes to {fn}")
        write_file_as_root(fn, KALI_SSH_WIDE_COMPAT_CONTENT)

    def unset_wide_compat(self):
        fn = self.kali_conf
        print("> Disabling wide compatibility")
        print(f"> Removing {fn}")
        run_as_root(f"rm -f {fn}", log=False)
